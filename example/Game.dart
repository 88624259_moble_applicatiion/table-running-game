// ignore_for_file: prefer_initializing_formals
import 'dart:io';
import 'dart:math';
import 'Back.dart';
import 'BlackHole.dart';
import 'Jump.dart';
import 'Player.dart';
import 'Map.dart';
import 'Superjump.dart';

class Game {
  var B = Back(3);
  var BH = BlackHole(14);
  var J = Jump(2);
  var SJ = Superjump(7);
  int dice = 0;
  Map? map;
  late Player player;
  static Random random = Random();

  Game(Map map, Player player) {
    this.map = map;
    this.player = player;
  }
  void walk() {
    var r = randomDice();
    print("You random is: $r  Point");
    var w = player.index + r;
    if (w > 47) {
      player.setMap(47);
      printMap();
      checkWinner();
    } else {
      player.setMap(w);
    }
    checkWalk();
    //return "You random is: $r  Point";
  }

  String checkWalk() {
    var index = player.getIndex();
    if (map!.tableMap[index] == "BH") {
      print('---You encounter Black!! Hole---');
      var p = index - BH.getValue();
      player.setMap(p);
      print('---You were sucked back : ${BH.value} Table---');
    } else if (map!.tableMap[index] == "B") {
      print('---You encounter Black!! ---');
      var p = index - B.getValue();
      player.setMap(p);
      print('---You were sucked back : ${B.value} Table---');
    } else if (map!.tableMap[index] == "J") {
      print('---You encounter bonus Jump!!---');
      var p = index + B.getValue();
      player.setMap(p);
      print('---You jumped over : ${J.getValue()} Table---');
    } else if (map!.tableMap[index] == "SJ") {
      print('---You encounter bonus Super Jump!!---');
      var p = index + B.getValue();
      player.setMap(p);
      print('---You jumped over : ${SJ.getValue()} Table---');
    }
    checkWinner();
    return "";
  }

  void addPlayerToMap(Player player) {
    //map?.addMap(player);
  }

  bool checkWinner() {
    if (player.getIndex() >= 47) {
      printFinsh();
      player.winner = true;
      return false;
    }
    printMap();
    return true;
  }

  int randomDice() {
    int min = 1, max = 6;
    int num = min + random.nextInt(max - min);
    return num;
  }

  String printFinsh() {
    return '-------------Finish Table Running Game-----------';
  }

  void printMap() {
    var s;
    print(
        "x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-");
    for (int i = 0; i < 48; i++) {
      if (i != player.getIndex()) {
        stdout.write(map!.tableMap[i] + "  ");
      } else {
        stdout.write("${player.playerName}" + " ");
      }
    }
    print(
        "x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x-x--x-x-x-x-x-x-x-x-x-");

    //return " value of Dice is: $dice";
  }
}
