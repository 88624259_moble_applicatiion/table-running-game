// ignore_for_file: prefer_initializing_formals

import 'Back.dart';

class Superjump extends Back {
  String symbol = "SJ";
  int value = 4;

  Superjump(int value) : super(value) {
    this.symbol = symbol;
    this.value = value;
  }
}
