// ignore_for_file: prefer_initializing_formals

import 'Back.dart';

class Jump extends Back {
  String symbol = "SJ";
  int value = 1;

  Jump(int value) : super(value) {
    this.symbol = symbol;
    this.value = value;
  }
}
