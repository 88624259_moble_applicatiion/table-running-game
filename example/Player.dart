// ignore_for_file: prefer_initializing_formals

import 'Medel_obj.dart';

class Player extends Model_obj {
  String? playerName;
  int index = 0;
  bool winner = false;

  Player(String name, int value) : super(value) {
    this.playerName = name;
    this.index = value;
  }

  String? getName() {
    return playerName;
  }

  int getIndex() {
    return index;
  }

  void setPlayerName(String value) {
    playerName = value;
  }

  void setMap(int value) {
    index = value;
  }
}
