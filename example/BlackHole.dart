import 'Medel_obj.dart';

class BlackHole extends Model_obj {
  String symbol = "BH";
  int value = 10;

  BlackHole(int value) : super(value) {
    this.symbol = symbol;
    this.value = value;
  }
}
