// ignore_for_file: prefer_initializing_formals

import 'dart:io';

import 'Arrested.dart';
import 'Back.dart';
import 'BlackHole.dart';
import 'Medel_obj.dart';
import 'Player.dart';
import 'Superjump.dart';
import 'jump.dart';

class Map {
  int? index;
  List<String> tableMap = [
    'start',
    '-',
    'SJ',
    '-',
    'SJ',
    '-',
    '-',
    'B',
    '-',
    '-',
    'SJ',
    '-',
    '-',
    '-',
    'J',
    '-',
    '-',
    '-',
    'SJ',
    'BH',
    '-',
    'BH',
    '-',
    'B',
    '-',
    '-',
    '-',
    'SJ',
    '-',
    '-',
    '-',
    'J',
    '-',
    '-',
    'BH',
    '-',
    '-',
    '-',
    '-',
    'B',
    '-',
    '-',
    '-',
    'BH',
    '-',
    'J',
    '-',
    'Finish'
  ];
  Back? back;
  Superjump? superjump;
  Jump? jump;
  BlackHole? blackhole;
  Arrested? arrested;
  /*
  void addMap(Model_obj obj, int index) {
    tableMap![index] = obj;
  }*/

  void setJump(int index, int newIndex) {
    tableMap[newIndex] = tableMap[index];
    tableMap.removeAt(index);
  }

  /*void setPositionPlayerInMap(Player player, int Lastindex, int Newindex) {
    player.setIndex(Newindex);
    tableMap[Newindex] = tableMap[Lastindex];
    tableMap.removeAt(Lastindex);
  }*/
}
